package be.ordina.smals.magicsquare;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MagicSquareTest {

    private MagicSquare square;

    private int size = 77;


    @Test
    void valueAtPosition() {
        square = new MagicSquare(3);
        MatcherAssert.assertThat(square.valueAtPosition(0,1), CoreMatchers.is(1));
        MatcherAssert.assertThat(square.valueAtPosition(1,2), CoreMatchers.is(7));
    }

    @Test
    void rowSum() {
        square = new MagicSquare(size);
        int baseline = square.colSum(0);
        for (int i = 1; i < size; i++){

            MatcherAssert.assertThat(square.rowSum(i), CoreMatchers.is(baseline));
        }
    }

    @Test
    void colSum() {
        square = new MagicSquare(size);
        int baseline = square.colSum(0);
        for (int i = 1; i < size; i++){

            MatcherAssert.assertThat(square.colSum(i), CoreMatchers.is(baseline));
        }
    }
}
