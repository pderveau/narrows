package be.ordina.smals.fizzbuzz;

import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;


class FizzBuzzPrinterTest {

    private FizzBuzzPrinter printer = new FizzBuzzPrinter();

    @Test
    void fizzBuzz() {

        MatcherAssert.assertThat(printer.fizzBuzz(2), is("2"));
        MatcherAssert.assertThat(printer.fizzBuzz(9), is("Fizz"));
        MatcherAssert.assertThat(printer.fizzBuzz(10), is("Buzz"));
        MatcherAssert.assertThat(printer.fizzBuzz(15), is("FizzBuzz"));

    }


}
