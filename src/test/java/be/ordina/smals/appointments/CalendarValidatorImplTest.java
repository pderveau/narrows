package be.ordina.smals.appointments;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@ExtendWith(MockitoExtension.class)
class CalendarValidatorImplTest {

    @InjectMocks
    private CalendarValidatorImpl validator;


    private List<Date> dateList;

    @BeforeEach
    public void init() {
        LocalDateTime now = LocalDateTime.now();
        Appointment app1 = new Appointment(UUID.randomUUID(), now, now.plusHours(1));
        Appointment app2 = new Appointment(UUID.randomUUID(), now.plusHours(1), now.plusHours(2));
        Appointment app3 = new Appointment(UUID.randomUUID(), now.plusHours(2), now.plusHours(3));

        dateList = new ArrayList<>();
        dateList.addAll(Arrays.asList(
                        convertLocalToDate(app1.getStartTime()),
                        convertLocalToDate(app1.getEndTime()),
                        convertLocalToDate(app2.getStartTime()),
                        convertLocalToDate(app2.getEndTime()),
                        convertLocalToDate(app3.getStartTime()),
                        convertLocalToDate(app3.getEndTime())
                        ));

    }


    @Test
    void validateAppointments_AllOk() {
        validator.validateAppointments(dateList.toArray(new Date[dateList.size()]));
    }


    @Test
    void validateAppointments_Uneven() {

        dateList.add(Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()));

        ValidationException thrown = Assertions.assertThrows(ValidationException.class, () -> {
            validator.validateAppointments(dateList.toArray(new Date[dateList.size()]));
        });

        Assertions.assertEquals("Number of Appointments in input is not an even number", thrown.getMessage());
    }

    @Test
    void validateAppointments_InvalidAppointment() {

        dateList.add(Date.from(LocalDateTime.now().plusHours(10).atZone(ZoneId.systemDefault()).toInstant()));
        dateList.add(Date.from(LocalDateTime.now().plusHours(9).atZone(ZoneId.systemDefault()).toInstant()));

        ValidationException thrown = Assertions.assertThrows(ValidationException.class, () -> {
            validator.validateAppointments(dateList.toArray(new Date[dateList.size()]));
        });

        Assertions.assertEquals("Appointment end date cannot be before start date", thrown.getMessage());
    }

    @Test
    void validateAppointments_Overlap() {

        dateList.add(Date.from(LocalDateTime.now().plusHours(1).atZone(ZoneId.systemDefault()).toInstant()));
        dateList.add(Date.from(LocalDateTime.now().plusHours(3).atZone(ZoneId.systemDefault()).toInstant()));

        ValidationException thrown = Assertions.assertThrows(ValidationException.class, () -> {
            validator.validateAppointments(dateList.toArray(new Date[dateList.size()]));
        });

        Assertions.assertEquals("Appointment overlaps", thrown.getMessage());
    }



    private Date convertLocalToDate(LocalDateTime time) {
        return Date.from(time
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }
}
