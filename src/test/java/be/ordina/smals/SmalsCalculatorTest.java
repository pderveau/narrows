package be.ordina.smals;

import be.ordina.smals.calculator.Calculator;
import be.ordina.smals.calculator.SmalsCalculator;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SmalsCalculatorTest {

    private Calculator calc = new SmalsCalculator();


    @Test
    void add() {

        assertThat(calc.add(3,5).doubleValue(), is(new BigDecimal(8.0).doubleValue()));
        assertTrue(calc.add(3,5).compareTo(new BigDecimal(8.0)) == 0);

    }

    @Test
    void subtract() {
        assertThat(calc.subtract(3,5).doubleValue(), equalTo(new BigDecimal(-2.0).doubleValue()));
    }

    @Test
    void multiply() {
        assertThat(calc.multiply(3,5).doubleValue(), is(new BigDecimal(15.0).doubleValue()));
    }

    @Test
    void divide() {
        assertThat(calc.divide(3,5).doubleValue(), is(new BigDecimal(0.6).doubleValue()));
    }
}
