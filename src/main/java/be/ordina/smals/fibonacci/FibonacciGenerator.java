package be.ordina.smals.fibonacci;

public class FibonacciGenerator {

    public static void main(String[] args) {

        // maxNumber of fibonacci numbers

        int maxNumber = 10;
        System.out.print("Fibonacci Series of " + maxNumber + " numbers: ");
        for (int i = 0; i < maxNumber; i++) {
            System.out.print(fibonacciRecursion(i) + " ");
        }
        System.out.println("");


        // max out at given number

        int fibo = 0;
        int index = 0;
        int max = 34;

        do {
            fibo = fibonacciRecursion(index++);
            if (fibo <= max) {
                System.out.println(String.format("Index : %s \t Fibo : %s ", index, fibo));
            }

        } while (fibo < max);


        // maxNumber of fibonacci numbers without recursion

        fibonacciLoop(10);


    }


    public static int fibonacciRecursion(int n) {
        if (n == 0) {
            return 0;
        }

        if (n == 1 || n == 2) {
            return 1;
        }


        int result = fibonacciRecursion(n - 2) + fibonacciRecursion(n - 1);
        return result;
    }


    private static void fibonacciLoop(int maxNumber) {

        int previousNumber = 0;
        int nextNumber = 1;


        System.out.print("Fibonacci Series of " + maxNumber + " numbers:");

        for (int i = 0; i < maxNumber; i++) {

            System.out.print(previousNumber + " ");

            int sum = previousNumber + nextNumber;
            previousNumber = nextNumber;
            nextNumber = sum;
        }


    }

}
