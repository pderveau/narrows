package be.ordina.smals;


import be.ordina.smals.fizzbuzz.FizzBuzzPrinter;
import be.ordina.smals.sentences.SentenceTilter;
import be.ordina.smals.appointments.*;
import be.ordina.smals.wordcount.WordCounter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class MainClass {

    public static void main(String... args) {

        SentenceTilter tilter = new SentenceTilter();
        //tilter.tiltSentences(Arrays.asList("Test deze eens", "En doe deze ook maar"));

//        Calculator calc = new SmalsCalculator();
//        System.out.println("output : " + calc.add(7, 19));
//        System.out.println("output : " + calc.subtract(7, 19));
//        System.out.println("output : " + calc.multiply(7, 19));
//        System.out.println("output : " + calc.divide(7, 19));

//        CalendarValidator validator = new CalendarValidatorImpl();
//
//        LocalDateTime now = LocalDateTime.now();
//        Date time1 = getDate(now);
//        Date time2 = getDate(now.plusHours(1));
//        Date time3 = getDate(now.plusHours(1));
//        Date time4 = getDate(now.plusHours(2));
//        Date time5 = getDate(now.plusHours(2));
//        Date time6 = getDate(now.plusHours(3));
//        Date time7 = getDate(now.plusHours(3));
//        Date time8 = getDate(now.plusHours(4));
//
//        validator.validateAppointments(time1, time2, time3, time4, time5, time6, time7, time8);

//        WordCounter wordCounter = new WordCounter("Don't take life too seriously; you'll never get out of it alive ");
//        wordCounter.orderWordsBySize();

//        FizzBuzzPrinter fizzBuzzPrinter = new FizzBuzzPrinter();
//        fizzBuzzPrinter.run(100);


    }

    private static Date getDate(LocalDateTime now) {
        return Date.from(now.atZone(ZoneId.systemDefault())
                .toInstant());
    }


}
