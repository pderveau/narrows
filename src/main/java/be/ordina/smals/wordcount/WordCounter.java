package be.ordina.smals.wordcount;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WordCounter {

    private String input;

    public WordCounter() {
    }

    public WordCounter(String input) {
        this.input = input;
    }

    public void orderWordsBySize(){

        // remove punctuation but keep whitespace
        input = input.replaceAll("[^a-zA-Z ]", "");


        // split sentence into segemnts
        List<String> words = Stream.of(input.split(" ")).collect(Collectors.toList());

        Map<Integer, Integer> lengthMap = new HashMap<>();

        words.forEach(
                word -> {
                    int size = word.length();
                    int currentNumber = lengthMap.get(size) != null ? lengthMap.get(size) : 0;
                    lengthMap.put(size, currentNumber+1);
                }
        );

        lengthMap.forEach( (length,number) -> {
            System.out.println(String.format("There are %s words with %s letters", number, length));
        });
    }

}
