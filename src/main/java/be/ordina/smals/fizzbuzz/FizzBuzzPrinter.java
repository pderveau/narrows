package be.ordina.smals.fizzbuzz;

public class FizzBuzzPrinter {


    public void run(int max){
        for (int i = 1; i <= max; i++){
            print(i, fizzBuzz(i));
        }

    }

    public String fizzBuzz(int number){
            if (number % 3 == 0 && number % 5 == 0){
                return "FizzBuzz";

            } else if (number % 3 == 0){
                return "Fizz";

            } else if (number % 5 == 0){
                return "Buzz";

            } else {
                return ""+number;

            }
    }

    private void print(int index, String output){
        System.out.println(index + "\t ->" + output);
    }




}
