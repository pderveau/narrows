package be.ordina.smals.magicsquare;

import lombok.Getter;

@Getter
public class MagicSquare {

    private final int[][] field;
    private final int maxIndex;
    private final int size;

    public MagicSquare(int size) {
        this.field = new int[size][size];
        this.size = size;
        maxIndex = size - 1;

        fillSquare(size);

    }


    public int valueAtPosition (int row, int col){
        return field[row][col];
    }

    public int rowSum(int row){
        int sum = 0;
        for (int i = 0; i < size; i++){
            sum += field[row][i];
        }
        return sum;
    }

    public int colSum(int col){
        int sum = 0;
        for (int i = 0; i < size; i++){
            sum += field[i][col];
        }
        return sum;
    }





    private void fillSquare(int size) {

        int total = size * size;

        // determine middle column
        int middle = (size + 1) / 2;

        // set middle column to 1, account for 0 index
        int currentCol = middle - 1;
        int currentRow = 0;

        field[0][middle - 1] = 1;

        // fill rest of square
        for (int value = 2; value <= total; value++) {

            // calculate next column and row
            currentCol = calculateNextColumn(currentCol);
            currentRow = caluclateNextRow(currentRow);

            // secondary logic if next vector is already taken
            // remember that row and col already have updated values
            if (field[currentRow][currentCol] != 0) {
                currentCol = calculateSecondaryColumn(currentCol);
                currentRow = calculateSecondaryRow(currentRow);
            }

            // set value
            field[currentRow][currentCol] = value;
        }

        printSquare();


    }


    private int calculateNextColumn(int current) {
        // move one right or wrap around
        if (current == maxIndex) {
            return 0;
        } else {
            return current + 1;
        }
    }


    private int caluclateNextRow(int current) {
        // move one up or wrap around
        if (current == 0) {
            return maxIndex;
        } else {
            return current - 1;
        }
    }

    private int calculateSecondaryColumn(int currentCol) {
        // back down 1 or wrap around
        if (currentCol == 0) {
            return maxIndex;
        } else {
            return currentCol - 1;
        }
    }


    private int calculateSecondaryRow(int currentRow) {
        // back down 1 or wrap around
        if (currentRow == maxIndex) {
            return 1;
        } else {
            return currentRow + 2;
        }
    }


    private void printSquare() {
        for (int row = 0; row < field[0].length; row++) {
            for (int col = 0; col < field[0].length; col++) {
                System.out.print(field[row][col] + "  ");
            }
            System.out.println("");
        }
        System.out.println("\n");
    }

}
