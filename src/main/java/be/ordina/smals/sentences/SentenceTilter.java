package be.ordina.smals.sentences;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SentenceTilter {


    public void tiltSentences(List<String> sentences) {

        // determine 2D array row size
        int maxRows = determineMaxRows(sentences);

        // create 2D array
        String[][] outputLines = new String[maxRows][sentences.size()];

        // split sentences and add to 2d array
        for (int col = 0; col < sentences.size(); col++) {
            List<String> segments = getSegments(sentences.get(col));

            for (int row = 0; row < segments.size(); row++) {
                outputLines[row][col] = segments.get(row);
            }
        }

        // print 2D array
        printOutput(sentences, outputLines);

    }


    private List<String> getSegments(String s) {
        return Stream.of(s.split(" "))
                .map(segment -> new String(segment))
                .collect(Collectors.toList());
    }

    private int determineMaxRows(List<String> sentences) {
        int max = 0;

        for (String sentence : sentences) {
            List<String> segments = getSegments(sentence);
            if (segments.size() > max) {
                max = segments.size();
            }
        }
        return max;
    }


    private void printOutput(List<String> sentences, String[][] outputLines) {

        for (int row = 0; row < outputLines.length; row++) {
            for (int col = 0; col < sentences.size(); col++) {
                if (outputLines[row][col] == null) {
                    System.out.print("\t\t\t");
                } else if (col < sentences.size()-1) {
                    System.out.print(outputLines[row][col] + " \t\t");
                } else {
                    System.out.print(outputLines[row][col] + "");
                }
            }
            System.out.print("\n");
        }
    }


}
