package be.ordina.smals;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmalsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmalsApplication.class, args);
    }

}
