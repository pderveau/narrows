package be.ordina.smals.calculator;

import java.math.BigDecimal;

public interface Calculator {

//    BigDecimal add(BigDecimal term1, BigDecimal term2);
//    BigDecimal subtract(BigDecimal term1, BigDecimal term2);
//    BigDecimal multiply(BigDecimal term1, BigDecimal term2);
//    BigDecimal divide(BigDecimal term1, BigDecimal term2);

    BigDecimal add(Number term1, Number term2);
    BigDecimal subtract(Number term1, Number term2);
    BigDecimal multiply(Number term1, Number term2);
    BigDecimal divide(Number term1, Number term2);
}
