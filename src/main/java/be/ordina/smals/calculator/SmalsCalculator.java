package be.ordina.smals.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SmalsCalculator implements Calculator{

    @Override
    public BigDecimal add(Number term1, Number term2){
        BigDecimal append1 = getBigDecimal(term1);
        BigDecimal append2 = getBigDecimal(term2);

        return append1.add(append2);
    }


    @Override
    public BigDecimal subtract(Number term1, Number term2) {
        BigDecimal append1 = getBigDecimal(term1);
        BigDecimal append2 = getBigDecimal(term2);

        return append1.subtract(append2);
    }

    @Override
    public BigDecimal multiply(Number term1, Number term2) {
        BigDecimal append1 = getBigDecimal(term1);
        BigDecimal append2 = getBigDecimal(term2);

        return append1.multiply(append2);
    }

    @Override
    public BigDecimal divide(Number term1, Number term2) {
        BigDecimal append1 = getBigDecimal(term1);
        BigDecimal append2 = getBigDecimal(term2);

        return append1.divide(append2, 5, RoundingMode.HALF_UP);
    }



    private BigDecimal getBigDecimal(Number term1) {
        return BigDecimal.valueOf(term1.doubleValue());
    }


}
