package be.ordina.smals.appointments;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class CalendarValidatorImpl implements CalendarValidator {


    @Override
    public void validateAppointments(Date... inputDates) {

        validateInputisEven(inputDates);

        List<Appointment> appointmentList = convertInputDatesToAppointment(inputDates);

        validateEndIsAfterStart(appointmentList);

        validateAppointmentsDoNotOverlap(appointmentList);


    }

    private void validateAppointmentsDoNotOverlap(List<Appointment> appointmentList) {
        System.out.println("--- Checking overlap");
        appointmentList.forEach(
                appointment -> {
                    checkOverlap(appointmentList, appointment);
                });
    }

    private void checkOverlap(List<Appointment> appointmentList, Appointment appointment) {

        appointmentList.removeIf(element -> element.equals(appointment));
        appointmentList.stream()
                .forEach(
                        appToCheck -> {
                            if (!appointment.getStartTime().isAfter(appToCheck.getEndTime()) && !appToCheck.getStartTime().isAfter(appointment.getEndTime())) {

                                System.out.println(String.format("Appointment %s overlaps with appointment %s", appointment, appToCheck));
                                throw new ValidationException("Appointment overlaps");
                            }
                        }
                );


    }

    //TODO : use validator classes
    private void validateEndIsAfterStart(List<Appointment> appointmentList) {
        System.out.println("--- Checking start and end order");
        appointmentList.forEach(
                appointment -> {
                    if (!appointment.getStartTime().isBefore(appointment.getEndTime())) {
                        throw new ValidationException("Appointment end date cannot be before start date");
                    } else {
                        System.out.println("Appn start " + appointment.getStartTime() + " is before Appn end " + appointment.getEndTime());
                    }
                }
        );

    }


    private void validateInputisEven(Date[] appointments) {
        System.out.println("--- Checking even number of elements");
        if (appointments.length % 2 != 0) {
            throw new ValidationException("Number of Appointments in input is not an even number");
        }

    }


    private List<Appointment> convertInputDatesToAppointment(Date[] inputDates) {

        List<Appointment> appointments = new ArrayList<>();

        // process 2 at a time
        for (int i = 0; i < inputDates.length; i += 2) {

            LocalDateTime start = getLocalDateTime(inputDates[i]);
            LocalDateTime end = getLocalDateTime(inputDates[i + 1]);

            appointments.add(new Appointment(UUID.randomUUID(), start, end));

        }

        return appointments;


    }

    private LocalDateTime getLocalDateTime(Date inputDate) {
        return inputDate.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }


}
