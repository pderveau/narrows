package be.ordina.smals.appointments;

public class ValidationException extends RuntimeException {

    public ValidationException() {
        super();
    }

    public ValidationException(String s) {
        super(s);
    }
}
