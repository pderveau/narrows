package be.ordina.smals.appointments;

import java.util.Date;

public interface CalendarValidator {

    void validateAppointments (Date... appointments);

}
